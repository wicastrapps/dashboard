const Koa = require('koa')
const Router = require('koa-router')
const bodyParser = require('koa-bodyparser')
const jwt = require('koa-jwt')
const http = require('http')
const io = require('./api/socket/socket')
const config = require('./config/config')
const jwtConfig = require('./config/jwtConf')
const Serve = require('koa-static')
//  const exec = require('child_process').exec;

//      var child = exec(`sh test.sh` ,
//      (error, stdout, stderr) => {
//          console.log(`${stdout}`);
//          console.log(`${stderr}`);
//          if (error !== null) {
//              console.log(`exec error: ${error}`);
//             }
//         })

const app = new Koa()

app.use(bodyParser())

app.use(async (ctx, next) => {
	ctx.set('Access-Control-Allow-Origin', '*')
	ctx.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT')
	ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, authorization')
	if (ctx.method === 'OPTIONS') {
		ctx.status = 204
	} else {
		await next()
	}
})

app.use(async (ctx, next) => {
	try {
		await next()
	} catch (err) {
		ctx.status = err.status ? err.status : 500
		ctx.body = err.message ? err.message : 'Server error'
		console.log(err.message, err.name, err.status)
	}
})

app.use(Serve('./public'))

const server = http.createServer(app.callback())

io.socket(server)

const mainRouter = new Router()
mainRouter.get('/', async (ctx, next) => {
	ctx.status = 200
	ctx.body = 'Connected'
})

// Load routes
const settingsRouter = require('./api/routes/settings')
const loginRouter = require('./api/routes/login')

app.use(mainRouter.routes()).use(mainRouter.allowedMethods())

// Middleware below this line is only reached if JWT token is valid
app.use(jwt({ secret: jwtConfig.secret }).unless({ path: [/^\/login/] }))

app.use(loginRouter.routes())
app.use(settingsRouter.routes())

server.listen(config.port)
