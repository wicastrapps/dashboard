const config = require('../../config/config')
const socket = require('socket.io-client')(config.clientHost)

socket.on('connect', function () {
	console.log('Connected...')
})
module.exports.device_status = function (event, data) {
	socket.emit(event, { status: data })
	return 'Emmitted event is- ' + event + ' ,status is- ' + data
}
socket.on('vpn', function () {
	console.log('Disconnected...')
	socket.disconnect()
})

socket.on('con', function () {
	console.log('Disconnected...')
	socket.disconnect()
})

require('make-runnable')
