const Joi = require('joi')

module.exports = Joi.object().keys({
	name: Joi.string().alphanum().min(3),
	password: Joi.string().regex(/^[a-zA-Z0-9]{8,20}$/)
})
