const User = require('../models/user')
const jwtConf = require('../../config/jwtConf')
const jwt = require('jsonwebtoken')

function jwtSignUser (user) {
	const exp = jwtConf.expiration
	const secret = jwtConf.secret
	return jwt.sign(user, secret, {
		expiresIn: exp
	}
	)
}

module.exports = async (ctx, next) => {
	const { name, password } = ctx.request.body
	const auth = new User()
	const isUserValid = auth.validation(name, password)
	if (isUserValid) {
		const token = jwtSignUser(ctx.request.body)
		ctx.body = 'Bearer ' + token
	} else {
		ctx.throw(401, 'Wrong name or password')
	}
}
