const Router = require('koa-router')
const settingsRouter = new Router()

const gsmSetts = require('../controllers/gsmSettings')
const lanSetts = require('../controllers/lanSettings')

settingsRouter.post('/gsmsettings', gsmSetts)
settingsRouter.post('/lansettings', lanSetts)

module.exports = settingsRouter
