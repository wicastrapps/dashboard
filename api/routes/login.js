const Router = require('koa-router')
const loginCtrl = require('../controllers/login')
const validate = require('../JoiSchemas/index')
const loginSchema = require('../JoiSchemas/loginSchema')
const loginRouter = new Router()

loginRouter.post('/login', validate('body', loginSchema), loginCtrl)

module.exports = loginRouter
