const Socket = require('socket.io')

module.exports.socket = (server) => {
	const io = new Socket(server)

	let vpn_stat = { status: 'off' }
	let con_stat = { status: 'Offline' }

	io.on('connection', function (socket) {
		console.log('user connected ' + socket.id)
		console.log(io.engine.clientsCount)
		//  if(io.engine.clientsCount>2){
		//         socket.disconnect(true);
		//         console.log("disconected")
		//      }
		// io.emit('vpn',{status:"on"})

		io.emit('vpn', vpn_stat)
		io.emit('con', con_stat)

		socket.on('vpn', function (state) {
			vpn_stat = state
			io.emit('vpn', vpn_stat)
		})

		socket.on('con', function (state) {
			con_stat = state
			io.emit('con', con_stat)
		})
	})
}
